const canvas = document.querySelector("#screen");
const ctx = canvas.getContext("2d");

const grey = "#394244";
const green = "#00ff14";

let sinShadow = [];
let angles = [0, 0];
let anglesFreq = [0.12, 0.04];
let posValues = [0, 0];


/**** ---- utility ---- ****/

//@args = { x1, y1, x2, y2, width, color } 
const drawLine = (args) => {
	ctx.beginPath();
	ctx.moveTo(args.x1, args.y1);
	ctx.lineTo(args.x2, args.y2);
	ctx.lineWidth = args.width;
	ctx.strokeStyle = args.color;
	ctx.stroke();
	ctx.closePath();
}

//@args = { x, y, r, color } 
const drawCircle = (args) => {
	ctx.beginPath();
	ctx.arc(args.x, args.y, args.r, 0, 2 * Math.PI);
	ctx.fillStyle = args.color;
	ctx.fill();
	ctx.closePath();
}


/**** ---- core ---- ****/

const drawCoordinateLines = () => {
	drawLine({
		x1: canvas.width / 2,
		y1: 0,
		x2: canvas.width / 2,
		y2: canvas.height,
		width: 2,
		color: grey
	});

	drawLine({
		x1: 0,
		y1: canvas.height / 2,
		x2: canvas.width,
		y2: canvas.height / 2,
		width: 2,
		color: grey
	});
}

const drawGridLines = () => {
	const gap = 31.9;
	const gridCount = canvas.width / gap;
	
	for (let i = 0; i < gridCount; i++) {
		drawLine({
			x1: i * gap,
			y1: 0,
			x2: i * gap,
			y2: canvas.height,
			width: 1,
			color: "rgb(57, 66, 68, 0.5)"
		});

		drawLine({
			x1: 0,
			y1: i * gap,
			x2: canvas.width,
			y2: i * gap,
			width: 1,
			color: "rgb(57, 66, 68, 0.5)"
		});
	}
}

function WaveAnimation(x, y, color) {
	this.x = x;
	this.basey = y;
	this.color = color;
	this.animating = true;
	this.shadow = [];
}

// @val = sin or cos value depended on angle to change y 
WaveAnimation.prototype.update = function(val) {
	if (this.animating) {
		this.y = this.basey + val;
	
		this.shadow.push({x: this.x, y: this.y, xbase: this.x, o: 1});

		if (this.shadow.length > 300)
			this.shadow.shift();

		for (let obj of this.shadow) {	
			obj.x -= 1.2;
			obj.xbase -= 1.2;
			obj.o -= 0.0031;
		}
	}
}

WaveAnimation.prototype.draw = function() {
	for (let i = 0; i < this.shadow.length - 1; i++) {
		
		drawLine({
			x1: this.shadow[i].x,
			y1: this.shadow[i].y,
			x2: this.shadow[i + 1].x,
			y2: this.shadow[i + 1].y,
			width: 5,
			color: `rgba(${this.color[0]}, ${this.color[1]}, ${this.color[2]}, ${this.shadow[i].o})`
		});
	}
	
	drawCircle({
		x: this.x,
		y: this.y,
		r: 10,
		color: `rgba(${this.color[0]}, ${this.color[1]}, ${this.color[2]}, 1)`
	});
}


/**** ---- loop ---- ****/
const sinWavePos = {
	x: canvas.width / 2 + 130, 
	y: canvas.height / 2
}

const sinWave = new WaveAnimation(
	sinWavePos.x, 
	sinWavePos.y, 
	[160, 255, 255]
);

const cosWavePos = {
	x: canvas.width / 2 + 100, 
	y: canvas.height / 2
}

const cosWave = new WaveAnimation(
	cosWavePos.x, 
	cosWavePos.y,
	[255, 60, 255]
);

const waves = [sinWave, cosWave];

const update = () => {
	sinWave.update(140 * Math.sin(angles[0]));
	cosWave.update(140 * Math.cos(angles[1]));
	
	sinWave.x = sinWavePos.x + posValues[0];
	cosWave.x = cosWavePos.x + posValues[1];
	
	for (let i = 0; i < waves.length; i++) {
		if (waves[i].animating) angles[i] += anglesFreq[i];
	}
}

const draw = () => {
	drawGridLines();
	drawCoordinateLines();
	sinWave.draw();
	cosWave.draw();
}

function loop() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	draw();
	update();
	requestAnimationFrame(loop);
}


/**** ---- init values ---- ****/

for (let i = 0; i < 250; i++) {
	draw();
	update();
}

loop();


/**** ---- input range event binding ---- ****/

Array.from(document.querySelectorAll(".freqranges")).forEach((e) => {
	e.addEventListener("input", function() {
		let index = Number(this.getAttribute("data-index"));
		anglesFreq[index] = Number(this.value);
	});
});

Array.from(document.querySelectorAll(".posranges")).forEach((e) => {
	e.addEventListener("input", function() {
		let index = Number(this.getAttribute("data-index"));
		posValues[index] = Number(this.value);
		waves[index].animating = false;
		waves[index].shadow = [];
	});
	
	e.addEventListener("mouseup", function() {
		let index = Number(this.getAttribute("data-index"));
		waves[index].animating = true;
	});
});